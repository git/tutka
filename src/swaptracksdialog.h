/*
 * swaptracksdialog.h
 *
 * Copyright 2002-2019 Vesa Halttunen
 *
 * This file is part of Tutka.
 *
 * Tutka is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Tutka is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Tutka; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef SWAPTRACKSDIALOG_H
#define SWAPTRACKSDIALOG_H

#include "tutkadialog.h"

namespace Ui {
    class SwapTracksDialog;
}

class Song;

class SwapTracksDialog : public TutkaDialog
{
    Q_OBJECT

public:
    explicit SwapTracksDialog(QWidget *parent = NULL);
    ~SwapTracksDialog();

public slots:
    void makeVisible();
    void setSong(Song *song);
    void setBlock(unsigned int block);

private slots:
    void swap();

private:
    enum Area {
        AreaSong,
        AreaBlock,
        AreaTrack,
        AreaSelection
    };

    Ui::SwapTracksDialog *ui;
    Song *song;
    unsigned int block;
};

#endif // SWAPTRACKSDIALOG_H
