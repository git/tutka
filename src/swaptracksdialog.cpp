/*
 * swaptracksdialog.cpp
 *
 * Copyright 2002-2019 Vesa Halttunen
 *
 * This file is part of Tutka.
 *
 * Tutka is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Tutka is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Tutka; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include "song.h"
#include "swaptracksdialog.h"
#include "ui_swaptracksdialog.h"

SwapTracksDialog::SwapTracksDialog(QWidget *parent) :
    TutkaDialog(parent),
    ui(new Ui::SwapTracksDialog),
    song(NULL),
    block(0)
{
    ui->setupUi(this);

    connect(ui->pushButtonSwap, SIGNAL(clicked()), this, SLOT(swap()));
}

SwapTracksDialog::~SwapTracksDialog()
{
    delete ui;
}

void SwapTracksDialog::makeVisible()
{
    show();
    raise();
    activateWindow();
}


void SwapTracksDialog::setSong(Song *song)
{
    this->song = song;
}

void SwapTracksDialog::setBlock(unsigned int block)
{
    this->block = block;
}

void SwapTracksDialog::swap()
{
    int from = ui->spinBoxFrom->value() - 1;
    int to = ui->spinBoxTo->value() - 1;

    switch (ui->comboBoxArea->currentIndex()) {
    case AreaSong:
        song->swapTracks(from, to);
        break;
    case AreaBlock: {
        Block *block = song->block(this->block);
        block->swapTracks(from, to);
        break;
    }
    default:
        break;
    }
}
